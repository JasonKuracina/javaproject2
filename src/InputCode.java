
/**
 * Name:      InputCode
 * Purpose:   Used to handle event code translation
 */
public class InputCode 
{
	//Class Scope Variables
	// Device constants: 
	public static final int DEVICE_KEYBOARD = 0;
	public static final int DEVICE_MOUSE = 1;
	// Event Types:
	public static final int ET_DOWN = 1 << 0;
	public static final int ET_UP =   1 << 1;
	public static final int ET_MOVE = 1 << 2;
	public static final int ET_ALL = ET_DOWN | ET_UP | ET_MOVE;
	// Game related input codes:
	public static final int IC_FIRE = 0; // Fire a laser:
	public static final int IC_MOUSE_CLICK = 1; //
	public static final int IC_NUKE = 2; // Move mouse
	public static final int IC_MAX = 3;
	
	/**
	 * Name:      getEventTypeString()
	 * Purpose:   Returns what the event type is in string form
	 * Inputs:     int
	 * Outputs:   String
	 */
	public static String getEventTypeString(int eventType)
	{
		switch(eventType)
		{
			case ET_DOWN: return "ET_DOWN"; 
			case ET_UP:   return "ET_UP";
			case ET_MOVE: return "ET_MOVE";
		}
		return "ET_INVALID";
	}
	
	/**
	 * Name:      getInputCodeString()
	 * Purpose:   Returns what the input type is in string form
	 * Inputs:     int
	 * Outputs:   String
	 */
	public static String getInputCodeString(int code)
	{
		switch(code)
		{
			case IC_FIRE: return "IC_FIRE";
			case IC_MOUSE_CLICK: return "IC_MOUSE_CLICK";
			case IC_NUKE: return "IC_NUKE";
		}
		return "IC_INVALID";
	}
}
