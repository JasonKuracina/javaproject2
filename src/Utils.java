import java.util.concurrent.ThreadLocalRandom;

/**
 * Name:      Utils
 * Purpose:   Utility functions of commonly used bits of code
 */
public class Utils {
	/**
	 * Name:      randomGen()
	 * Purpose:   Generate a random number within the range provided
	 * Inputs:     int (x2)
	 * Outputs:   int
	 */
	public static int randomGen(int min, int max){
		int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1) ;
		return randomNum;
	}
	
	/**
	 * Name:      lerp()
	 * Purpose:   Interpolation function
	 * Inputs:     float (x3)
	 * Outputs:   float
	 */
	public static float lerp(float x, float y, float t)
	{
		return x + (y-x) * t;
	}
	
}
