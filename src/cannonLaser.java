import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Name:      CannonLaser
 * Purpose:   Object to represent the laser going from the cannon to the location of the mouse press
 */
public class CannonLaser extends GameObject {
	//CLASS WIDE SCOPE
	private float originX = 350;
	private float originY = 610;
	private float currentX = 347;
	private float currentY = 610; //mouse cursor co-ordinates
	private GameObject clickerBounds = null;
	
	//Accessors
	public void setCurrentX(int x) { currentX = x; }
	public void setCurrentY(int y) { currentY = y; }
	public float getOriginX() { return originX; }
	public float getOriginY() { return originY; }
	
	/**
	 * Name:      paint()
	 * Purpose:   Draws a red line from the tip of the cannon to the location of them mouse press
	 * Inputs:     Graphics
	 * Outputs:   Nothing
	 */
	public void paint(Graphics g)
	{
		//	paint(g);
		//FILL IN NOW!
		//	super.paint(g);
		g.setColor(Color.RED);
		//draw the line
		g.drawLine((int)originX, (int)originY, (int)currentX, (int)currentY);
		
		//g.drawRect(currentX, currentY, width, height);
		//g.drawOval(currentX, currentY, width, height);
	}
	
	
	/**
	 * Name:      OnInputEvent()
	 * Purpose:   Overridden method to record the new location for the (x, y) of the pointer location.
	 * Inputs:     InputEvent
	 * Outputs:   Nothing
	 */
	@Override
	public void OnInputEvent(InputEvent h) {
		if(h.getInputCode() == InputCode.IC_MOUSE_CLICK) {
			if(h.getEventType() == InputCode.ET_DOWN) {
				currentX = h.getMouseX();
				currentY = h.getMouseY();
				clickerBounds = new ClickerBounds(currentX, currentY, 25.0f);
			}
			else if (h.getEventType() == InputCode.ET_UP) {
				currentX = originX;
				currentY = originY;
				MainProjectFile.Destroy(clickerBounds);
				clickerBounds = null;
			}
		}
	}
}