import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.Random;
import java.awt.geom.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Name:      Meteor
 * Purpose:   Object that represents each meteor in the applet
 */
public class Meteor extends GameObject {
	//Class Scope Variables
	double xVelocity, yVelocity;
	int hitsLeft;
	int min = 0;
	int max = 0;
	float speed = 15;
	Bounds b = new Bounds();
	private boolean isTwin;

	// TBD: declare instance variables here�
	
	//Accessors
	public float getRadius() { return (b.Right - b.Left) / 2.0f; }
	public int getHitsLeft() { return hitsLeft; }
	public boolean isTwin() { return isTwin; }
	@Override
	public Bounds getBound() { return b; }
	
	//6-arg constructor, creates a meteor at the top of the screen
	public Meteor(double x, double y, double radius, double minVelocity, double maxVelocity, int hitsLeft) {
		super();
		b.Left = (float) (x - radius);
		b.top = (float) (y - radius);
		b.Right = (float) (x + radius);
		b.bottom = (float) (y + radius);

		this.hitsLeft = hitsLeft; // number of shots left to destroy it
		double vel = minVelocity + Math.random() * (maxVelocity - minVelocity), dir = 2 * Math.PI * Math.random();
		xVelocity = vel * Math.cos(dir);
		yVelocity = vel * Math.sin(dir);
	}
	//3-arg constructor to create child meteors when a meteor is broken
	public Meteor(Meteor mom, double deltaX,int hitsLeft) {
		super();
		isTwin = true;
		this.b = mom.b.copy();
		this.b.Right += deltaX;
		this.b.Left += deltaX;
		this.hitsLeft = hitsLeft;
		
		// TBD
		
	}
	
	/**
	 * Name:      update()
	 * Purpose:   Overridden method that moves the meteor up and down
	 * Inputs:     double
	 * Outputs:   Nothing
	 */
	@Override
	public void update(double deltaTime) {
		super.update(deltaTime);
		speed += 15 * deltaTime;
		b.top += ( speed * deltaTime);
		b.bottom += ( speed * deltaTime);
		if (b.Left < 0 - getRadius())
			b.Left += MainProjectFile.getInstance().getWidth() + 2 * getRadius();
		else if (b.Left > MainProjectFile.getInstance().getWidth() + getRadius())
			b.Left -= MainProjectFile.getInstance().getWidth() + 2 * getRadius();
	}
	
	/**
	 * Name:      paint()
	 * Purpose:   Paints the meteors to the Graphics object
	 * Inputs:     Graphics
	 * Outputs:   Nothing
	 */
	public void paint(Graphics g) {
		int size = (int) (b.Right - b.Left);
		g.setColor(Color.gray); // set color for the asteroid
		// draw the asteroid centered at (x,y)
		g.fillOval((int) b.Left,(int)b.top,size,size);
		g.setColor(Color.WHITE);
		g.drawString("Score: " + MainProjectFile.score, 5, 15);
	}
	
	/**
	 * Name:      OnEnter()
	 * Purpose:   Handles the collisions with this object, including being clicked.
	 *                 REVISION1:  Added score handling
	 * Inputs:     GameObject
	 * Outputs:   Nothing
	 */
	public void OnEnter(GameObject gameO) {
		//Remove the first / on the line below to comment out the debug code
		//*
		if(this == gameO)
			System.out.println("OOnet fail.");
		System.out.printf(" On Enter %d > %s %d \n", hashCode(), gameO.getClass().getName(), gameO.hashCode());//*/
		if(gameO instanceof ClickerBounds) {
		hitsLeft--;
		}
		
		if(hitsLeft == 0) {
			MainProjectFile.score += 1;
			if(!isTwin) {
				new Meteor(this,(int) (-getRadius()  ), 3);
				new Meteor(this, (int) (getRadius() ), 3);
			}
			
			MainProjectFile.Destroy(this);
		}
	}
	
	/**
	 * Name:      OnExit
	 * Purpose:   Holds debug code relating to the destruction of objects
	 * Inputs:     GameObject
	 * Outputs:   Nothing
	 */
	public void OnExit(GameObject gameO) {
		if(this == gameO)
		{
			System.out.println("exit fail.");
		}
		
		System.out.printf(" On Exit %d > %s %d \n", hashCode(), gameO.getClass().getName(), gameO.hashCode());
		
	}
	// TBD

} // end class
