
// This class is created for the purpose of holding information to translate 
// native events from various devices into game input events.
// 
// See InputCode for constants related to the various variables in this class.
/**
 * Name:      InputBinding
 * Purpose:   Created for the purpose of holding information to translate native events from
 *                 various devices into game input events
 */
public class InputBinding 
{
	//Class Scope Variables
	// Game related input code this binding will generate events for.
	private int inputCode = 0;
	// Which device we will respond to
	private int deviceType = 0;
	// Which code (relative to the device) we will respond to.
	private int deviceCode = 0;
	// Event State response:
	private int eventTypeMask = 0;
	
	// Accessors:
	public int getInputCode() { return inputCode; }
	public int getDeviceType() { return deviceType; }
	public int getDeviceCode() { return deviceCode; }
	public int getEventTypeMask() { return eventTypeMask; }
	
	// 4-arg constructor
	public InputBinding(int inInputCode, int inDeviceType, int inDeviceCode, int inEventTypeMask)
	{
		super();
		inputCode = inInputCode;
		deviceType = inDeviceType;
		deviceCode = inDeviceCode;
		eventTypeMask = inEventTypeMask;
	}
	
	// Helper to check if we can use an event
	/**
	 * Name:      useEventType
	 * Purpose:   Kris fill the purpose of this in please, I dunno what its doing!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * Inputs:     int
	 * Outputs:   boolean
	 *                    true  -
	 *                    false -
	 */
	public boolean useEventType(int eventType) 
	{ 
		return (eventType & eventTypeMask) == eventType; 
	} 
}
