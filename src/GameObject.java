import java.awt.Graphics;

/**
 * Name:      GameObject
 * Purpose:   Base class for all objects in the game, it stores the (x, y) coordinates
 */
public class GameObject {
	//Class Scope Variables
	private double x = 0;
	private double y = 0;

	//Accessor
	public Bounds getBound() { return null; }
	
	//Default Constructor
	public GameObject() {
		MainProjectFile.RegisterGameObject(this);
		
	}
	
	/**
	 * Name:      OnInit()
	 * Purpose:   Method to be overridden by children objects of this
	 * Inputs:     Nothing
	 * Outputs:   Nothing
	 */
	public void OnInit() {}
	
	/**
	 * Name:      OnDestroy()
	 * Purpose:   Method to be overridden by children objects of this
	 * Inputs:     Nothing
	 * Outputs:   Nothing
	 */
	public void OnDestroy() {}
	
	/**
	 * Name:      OnEnter()
	 * Purpose:   Method to be overridden by children objects of this
	 * Inputs:     GameObject
	 * Outputs:   Nothing
	 */
	public void OnEnter(GameObject gameO) {}
	
	/**
	 * Name:      OnExit()
	 * Purpose:   Method to be overridden by children objects of this
	 * Inputs:     GameObject
	 * Outputs:   Nothing
	 */
	public void OnExit(GameObject gameO) {}
	
	/**
	 * Name:      update()
	 * Purpose:   Method to be overridden by children objects of this
	 * Inputs:     double
	 * Outputs:   Nothing
	 */
	public void update(double delt) {}
	
	/**
	 * Name:      paint()
	 * Purpose:   Method to be overridden by children objects of this
	 * Inputs:     Graphics
	 * Outputs:   Nothing
	 */
	public void paint(Graphics g) {}
	
	/**
	 * Name:      OnInputEvent()
	 * Purpose:   Method to be overridden by children objects of this
	 * Inputs:     InputEvent
	 * Outputs:   Nothing
	 */
	public void OnInputEvent(InputEvent h) {}

}
