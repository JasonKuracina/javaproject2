import java.awt.Graphics;
import java.util.Vector;

/**
 * Name:      Spawner
 * Purpose:   Class that handles the spawning of meteors
 */
public class Spawner extends GameObject{
	//Class Scope Variables
	double updateSeconds = 20.0;
	
	/**
	 * Name:      update()
	 * Purpose:   Spawns a meteor if a second has passed
	 * Inputs:     double
	 * Outputs:   Nothing
	 */
	public void update(double seconds){
		updateSeconds += seconds;
		if(updateSeconds >1) {
			createMeteor();
			updateSeconds = 0;
		}
	}
	
	/**
	 * Name:      createMeteor()
	 * Purpose:   Create a new meteor at a random x coordinate between 100 and 500,
	 *                 at the top of the screen
	 * Inputs:     Nothing
	 * Outputs:   Nothing
	 */
	public void createMeteor() { new Meteor(Utils.randomGen(100, 500), 0, 50, 100,100, Utils.randomGen(1, 4)); }
}
