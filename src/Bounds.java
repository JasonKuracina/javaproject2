/**
 * Name:      Bounds
 * Purpose:   Contains the information about the edges of an object
 */
public class Bounds {
	//Class scope variables
	public float Left;
	public float Right;
	public float top;
	public float bottom;
	
	/**
	 * Name:      intersection()
	 * Purpose:   Checks for an intersection between the Bound input and this
	 * Inputs:     Bounds
	 * Outputs:   boolean
	 *                      true   -  if there is an intersection between the objects
	 *                      false  -  No intersection between the objects
	 */
	public boolean intersection(Bounds b) {
		
		return!(b.Left > Right || 
				b.Right < Left ||
				b.top > bottom || 
				b.bottom < top);
		
	}
	
	/**
	 * Name:      copy()
	 * Purpose:   Create a copy of the current bound, and return it
	 * Inputs:     Nothing
	 * Outputs:   Bounds
	 */
	public Bounds copy() {
		Bounds b = new Bounds();
		b.Left = this.Left;
		b.Right = this.Right;
		b.top = this.top;
		b.bottom = this.bottom;
		
		return b;
		
	}
	
	
}
