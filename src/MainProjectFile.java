import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;

import javax.naming.Binding;
import javax.swing.ImageIcon;
import javax.swing.JApplet;

/**
 * Name:      MainProjectFile
 * Purpose:   Handles the bulk of the applet, such as the creation, and data pertaining to the current instance
 */
public class MainProjectFile extends JApplet implements MouseListener, MouseMotionListener, Runnable {
	//Class Scope Variables
	Vector<GameObject> objects = new Vector<GameObject>();
	Vector<GameObject> physicsObjects = new Vector<GameObject>();
	Vector<GameObject> onInitQueue = new Vector<GameObject>();
	Vector<GameObject> destroyQueue = new Vector<GameObject>();
	Vector<InputBinding> inputB = new Vector<InputBinding>();
	Vector<InputEvent> inputEC = new Vector<InputEvent>();
	Vector<Intersection> intersected = new Vector<Intersection>();
	Object inputLock = new Object();
	Shooter Cannon;
	Spawner spawn;
	Vector<Integer> inties = new Vector<Integer>();
	// CannonLaser laser = new CannonLaser(this);
	// Meteor sMeteor = new Meteor(this);
	Dimension dim;
	private Image backgroundFile;
	private Image Buffer;
	Graphics g;
	int randPosition;
	Meteor[] meteor; // the array of asteroids
	int numMeteors = 5; // the number of asteroids currently in the array
	double MeteorRadius, minMeteorVel, maxMeteorVel; // values used to create asteroids
	int MeteorNumHits, MeteorNumSplit;
	static MainProjectFile instance;
	static long score;

	/**
	 * Name:      Intersection
	 * Purpose:   Stored in a vector, used for collision testing
	 */
	private class Intersection {
		public GameObject a = null;
		public GameObject b = null;
		
		//2-arg constructor
		public Intersection(GameObject a, GameObject b) {
			this.a = a;
			this.b = b;

		}
	}

	//Getter
	public static MainProjectFile getInstance() {
		return instance;
	}

	//Methods
	
	/**
	 * Name:      RegisterGameObject()
	 * Purpose:   Add's the GameObject passed in to the current instance's onInitQueue and objects vectors
	 * Inputs:     GameObject
	 * Outputs:   Nothing
	 */
	public static void RegisterGameObject(GameObject object) {
		if (object == null) {
			return;
		}
		if (instance == null) {
			return;
		}
		instance.onInitQueue.add(object);
		instance.objects.add(object);
		
	}
	
	/**
	 * Name:      Destroy()
	 * Purpose:   Checks if the object exists, and removes it from the instance's objects vector, and adds it to the destroyQueue
	 * Inputs:     GameObject
	 * Outputs:   Nothing
	 */
	public static void Destroy(GameObject object) {
		if (object == null) {
			return;
		}
		if (instance == null) {
			return;
		}
		if (object.getBound() != null) {//never runs, object.getBound() only returns null
			instance.physicsObjects.remove(object);
		}
		int index = instance.objects.indexOf(object);
		if (index != -1) {
			instance.objects.remove(object);
			instance.destroyQueue.add(object);

		}
	}
	
	/**
	 * Name:       PollInput()
	 * Purpose:    Get the input events
	 * Inputs:     Nothing
	 * Outputs:   Nothing
	 */
	private void PollInput() {

		Vector<InputEvent> events = null;

		synchronized (inputLock) {
			events = inputEC;
			inputEC = new Vector<InputEvent>();

		}

		for (int i = 0; i < events.size(); ++i) {
			InputEvent temp = events.get(i);
			if ((temp.getEventType() != inties.get(temp.getInputCode()) || temp.getEventType() == InputCode.ET_MOVE)
					&& temp.getBinding().useEventType(temp.getEventType())) {

				for (int x = 0; x < objects.size(); ++x) {
					objects.get(x).OnInputEvent(events.get(i));

				}

			}
			inties.set(i, temp.getEventType());

		}

	}
	
	/**
	 * Name:      init()
	 * Purpose:   Build the main game screen for the applet.  Assigns key bindings and creates a buffer, then creates a new thread and starts it for the applet to run on
	 * Inputs:     Nothing
	 * Outputs:   Nothing
	 */
	public void init() {
		instance = this;
		this.setSize(700, 700);
		addMouseListener(this);
		addMouseMotionListener(this);
		backgroundFile = getImage(getCodeBase(), "earth.jpg");
		// Cannon = new Shooter();
		dim = getSize();
		Buffer = createImage(dim.width, dim.height);
		g = Buffer.getGraphics();
		// spawn = new Spawner();
		inputB.add(new InputBinding(InputCode.IC_FIRE, InputCode.DEVICE_MOUSE, MouseEvent.BUTTON1, InputCode.ET_UP));
		inputB.add(new InputBinding(InputCode.IC_MOUSE_CLICK, InputCode.DEVICE_MOUSE, MouseEvent.BUTTON1,
				InputCode.ET_DOWN | InputCode.ET_UP));
		inputB.add(new InputBinding(InputCode.IC_NUKE, InputCode.DEVICE_KEYBOARD, KeyEvent.VK_D, InputCode.ET_UP));
		for (int i = 0; i < InputCode.IC_MAX; ++i) {
			inties.add(InputCode.ET_UP);
		}
		Thread thread = new Thread(this);
		thread.start();
		score = 0;
	}
	
	/**
	 * Name:      paint()
	 * Purpose:   Fills over the current image and draws all the objects to the Graphics object
	 * Inputs:     Graphics
	 * Outputs:   Nothing
	 */
	public void paint(Graphics gfx) {
		// doubleG.drawImage(backgroundFile, 229, 449, this);

		g.setColor(Color.black);
		g.fillRect(0, 0, 700, 700);
		for (int i = 0; i < objects.size(); ++i) {
			objects.get(i).paint(g);
		}
		gfx.drawImage(Buffer, 0, 0, this);
	}
	
	/**
	 * Name:      update()
	 * Purpose:   Calls the paint function above
	 * Inputs:     Graphics
	 * Outputs:   Nothing
	 */
	public void update(Graphics gfx) {
		paint(gfx);
	}
	
	/**
	 * Name:      findIntersection()
	 * Purpose:   Compare 2  GameObjects with the values in the Vector<Intersection>
	 * Inputs:     GameObject (x2), Vector<Intersection>
	 * Outputs:   int
	 *                       1   -  Intersection found,
	 *                      -1   -  No intersection found
	 */
	public int findIntersection(GameObject a, GameObject b, Vector<Intersection> intersect) {

		for (int i = 0; i < intersect.size(); ++i) {

			Intersection c = intersect.get(i);
			if ((c.a == a && c.b == b) || (c.a == b && c.b == a)) {
				return 1;
			}
		}

		return -1;

	}
	// if it has an arg thats != to arg0 im using it for something..
	// have to implement
	
	/**
	 * Name:      run()
	 * Purpose:   The bread and butter of the applet.  This is an overridden run function that handles how the game objects spawn, how they
	 *                 interact with each other, and destroys them when needed.
	 * Inputs:     Nothing
	 * Outputs:   Nothing
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub

		long curr = System.nanoTime();
		long last = System.nanoTime();

		new PlayerClass();

		while (true) {
			last = curr;
			curr = System.nanoTime();
			long delta = curr - last;
			double seconds = Math.min((double) delta / 1000000000.0, 0.1);
			for (int i = 0; i < onInitQueue.size(); ++i) {
				GameObject current = onInitQueue.get(i);
				current.OnInit();
				if (current.getBound() != null) {
					instance.physicsObjects.add(current);

				}
			}
			onInitQueue.clear();
			for (int i = 0; i < objects.size(); ++i) {
				objects.get(i).update(seconds);

			}

			repaint();
			Vector<Intersection> onEnter = new Vector<Intersection>();
			Vector<Intersection> onExit = new Vector<Intersection>();
			Vector<Intersection> currIntersections = new Vector<Intersection>();
			for (int i = 0; i < physicsObjects.size(); ++i) {
				GameObject first = physicsObjects.get(i);
				for (int x = i; x < physicsObjects.size(); ++x) {
					if(i == x) {
						continue;
					}
					GameObject second = physicsObjects.get(x);
					if (second.getBound().intersection(first.getBound())) {
						currIntersections.add(new Intersection(first, second));
						if (findIntersection(first, second, intersected) == -1) {
							onEnter.add(new Intersection(first, second));
						}
					} else if (findIntersection(first, second, intersected) != -1) {
						onExit.add(new Intersection(first, second));
					}

				}

			}
			for (int i = 0; i < onEnter.size(); ++i) {;
				Intersection current = onEnter.get(i);
				current.a.OnEnter(current.b);
				current.b.OnEnter(current.a);

			}
			for (int i = 0; i < onExit.size(); ++i) {
				Intersection current = onExit.get(i);
				current.a.OnExit(current.b);
				current.b.OnExit(current.a);
			}
			
			intersected = currIntersections;
			
			PollInput();
			for (int i = 0; i < destroyQueue.size(); ++i) {
				GameObject current = destroyQueue.get(i);
				for(int x = 0; x < intersected.size(); ++x)
				{
					Intersection it = intersected.get(x);
					
					if(it.a == current || it.b == current)
					{
						it.b.OnExit(it.a);
						it.a.OnExit(it.b);
					}
				}
				current.OnDestroy();
			}
			destroyQueue.clear();

			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
	/**
	 * Name:      mouseDragged()
	 * Purpose:   Overridden method with no implementation
	 * Inputs:     MouseEvent
	 * Outputs:   Nothing
	 */
	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Name:      mouseMoved()
	 * Purpose:   Overridden method with no implementation
	 * Inputs:     MouseEvent
	 * Outputs:   Nothing
	 */
	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Name:      mouseClicked()
	 * Purpose:   Overridden method with no implementation
	 * Inputs:     MouseEvent
	 * Outputs:   Nothing
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Name:      mouseEntered()
	 * Purpose:   Overridden method with no implementation
	 * Inputs:     MouseEvent
	 * Outputs:   Nothing
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Name:      mouseExited()
	 * Purpose:   Overridden method with no implementation
	 * Inputs:     MouseEvent
	 * Outputs:   Nothing
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
	
	/**
	 * Name:      mousePressed()
	 * Purpose:   Overridden event that adds a mouse pressed event to inputEC with the (x, y) coordinate of the pointer
	 * Inputs:     MouseEvent
	 * Outputs:   Nothing
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		// laser.setCurrentX(e.getX());
		// laser.setCurrentY(e.getY());
		synchronized (inputLock) {
			for (int i = 0; i < inputB.size(); ++i) {
				InputBinding binding = inputB.get(i);
				if (binding.getDeviceType() == InputCode.DEVICE_MOUSE && binding.getDeviceCode() == e.getButton()) {
					inputEC.add(InputEvent.CreateMouseEvent(binding, InputCode.ET_DOWN, (float) e.getX(),
							(float) e.getY()));
				}

			}
		}
		// laserSound.play()
	}
	
	/**
	 * Name:      mouseReleased()
	 * Purpose:   Overridden event that adds a mouse release event to inputEC with the (x, y) coordinate of the pointer
	 * Inputs:     MouseEvent
	 * Outputs:   Nothing
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		// laser.setCurrentX(laser.getOriginX());
		// laser.setCurrentY(laser.getOriginY());
		synchronized (inputLock) {
			for (int i = 0; i < inputB.size(); ++i) {
				InputBinding binding = inputB.get(i);
				if (binding.getDeviceType() == InputCode.DEVICE_MOUSE && binding.getDeviceCode() == e.getButton()) {
					inputEC.add(
							InputEvent.CreateMouseEvent(binding, InputCode.ET_UP, (float) e.getX(), (float) e.getY()));
				}

			}
		}

	}
	// TODO Auto-generated method stub

}

/**
 * Name:      a
 * Purpose:   a
 * Inputs:     a
 * Outputs:   a
 */