import java.awt.Color;
import java.awt.Graphics;

/**
 * Name:      ClickerBounds
 * Purpose:   The clickable area of each GameObject that is acceptable to count as a shot
 */
public class ClickerBounds extends GameObject {
	//Class Scope Variables
	Bounds b = new Bounds();
	
	//Accessor
	public Bounds getBound() { return b; }
	
	//3-arg constructor
	public ClickerBounds(float x, float y, float size)
	{
		b.Left = x - size;
		b.top = y - size;
		b.Right = x + size;
		b.bottom = y + size;
	}
	
	/**
	 * Name:      paint()
	 * Purpose:   Overridden method that draws the clickable area to the Graphics object passed in
	 * Inputs:     Graphics
	 * Outputs:   Nothing
	 */
	@Override
	public void paint(Graphics g)
	{
		int size = (int)(b.Right-b.Left);
		g.setColor(Color.magenta);
		g.fillRect((int)b.Left, (int)b.top, size, size);
	}
	
	/**
	 * Name:      OnEnter()
	 * Purpose:   Overridden method that does not have an implementation
	 * Inputs:     GameObject
	 * Outputs:   Nothing
	 */
	public void OnEnter(GameObject gameO) {}
	
	/**
	 * Name:      OnExit()
	 * Purpose:   Overridden method that does not have an implementation
	 * Inputs:     GameObject
	 * Outputs:   Nothing
	 */
	public void OnExit(GameObject gameO) {}
}
