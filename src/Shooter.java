import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JApplet;

/**
 * Name:      Shooter
 * Purpose:   Class that defines the cannon that will be used to shoot meteors
 */
public class Shooter extends GameObject {
  //Default Constructor
	public Shooter() {}
	
	/**
	 * Name:      paint()
	 * Purpose:   Paints the cannon on the Graphics object
	 * Inputs:     Graphics
	 * Outputs:   Nothing
	 */
	public void paint(Graphics g) {
		g.setColor(Color.gray);
		g.fillRect(315, 650, 70, 10);
		g.fillRect(325, 640, 50, 10);
		g.fillRect(335, 630, 30, 10);
		g.fillRect(345, 620, 10, 10);
		g.setColor(Color.RED);
		g.fillRect(347, 610, 6, 10);
	}
	
	/**
	 * Name:      getBounds()
	 * Purpose:   Overridden method that returns the bounds for the shooter
	 * Inputs:     Nothing
	 * Outputs:   Bounds
	 */
	@Override
	public Bounds getBound()
	{
		Bounds b = new Bounds();
		b.Left = 0;
		b.Right = 1000.0f;
		b.top = 400.0f;
		b.bottom = 600.0f;
		return b;
		
	}
	
	
}
