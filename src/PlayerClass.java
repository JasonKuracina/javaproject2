/**
 * Name:      PlayerClass
 * Purpose:   Stores information about the instance of the game, such as the shooter and spawner
 */
public class PlayerClass extends GameObject {
  //Default Constructor
	public PlayerClass(){
		super();
	}
	
	/**
	 * Name:      OnInit()
	 * Purpose:   Does the initial set up for the player.  Creates the Shooter,
	 *                 CannonLaser, and Spawner.
	 * Inputs:     Nothing
	 * Outputs:   Nothing
	 */
	@Override
	public void OnInit() {
		super.OnInit();
		new Shooter();
		new CannonLaser();
		new Spawner();
		
		Bounds a = new Bounds();
		a.Left = 20;
		a.Right = 40;
		a.top = 20;
		a.bottom = 20;
		
		Bounds b = new Bounds();
		b.Left = 20;
		b.Right = 40;
		b.top = 25;
		b.bottom = 80;
		
		if(a.intersection(b))
		{
			System.out.println("failed");
		}
	}
	
	/**
	 * Name:      OnInputEvent()
	 * Purpose:   Overridden method being used for debugging
	 * Inputs:     InputEvent
	 * Outputs:   Nothing
	 */
	@Override
	public void OnInputEvent(InputEvent h) { System.out.printf("%s,%s \n", InputCode.getEventTypeString(h.getEventType()),InputCode.getInputCodeString(h.getInputCode())); }
}
