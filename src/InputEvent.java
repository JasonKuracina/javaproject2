/**
 * Name:      InputEvent
 * Purpose:   Provides information on various events
 */
public class InputEvent 
{
	//Class Scope Variables
	// InputCode this event is for.
	private InputBinding binding;
	// How the event was triggered.
	private int eventType = 0;
	// Which device sent this event.
	private int deviceType = 0;
	// Mouse device specific data.
	private float mouseX = 0.0f;
	private float mouseY = 0.0f;
	
	// Accessors
	public int getInputCode() { return binding != null? binding.getInputCode():-1; }
	public InputBinding getBinding() { return binding; }
	public int getEventType() { return eventType; }
	public int getDeviceType() { return deviceType; }
	public float getMouseX() { return mouseX; }
	public float getMouseY() { return mouseY; }
	
	/**
	 * Name:      CreateKeyEvent()
	 * Purpose:   Used to create InputEvents from a key press
	 * Inputs:     InputBinding, int
	 * Outputs:   InputEvent
	 */
	public static InputEvent CreateKeyEvent(InputBinding binding, int type)
	{
		InputEvent e = new InputEvent();
		e.binding = binding;
		e.eventType = type;
		e.deviceType = InputCode.DEVICE_KEYBOARD;
		return e;
	}
	
	/**
	 * Name:      CreateMouseEvent()
	 * Purpose:   Used to create InputEvents from a mouse press
	 * Inputs:     InputBinding, int, float (x2)
	 * Outputs:   InputEvent
	 */
	public static InputEvent CreateMouseEvent(InputBinding binding, int type, float x, float y)
	{
		InputEvent e = new InputEvent();
		e.binding = binding;
		e.eventType = type;
		e.deviceType = InputCode.DEVICE_MOUSE;
		e.mouseX = x;
		e.mouseY = y;
		return e;
	}
	
}
