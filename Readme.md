## Description
In this project, you will be coding a simple video game as an applet. The game is based on two disaster movies from the late 90’s: “Deep Impact”, and “Armageddon” (if you haven’t seen either one, watch “Deep Impact”…it was much better!). The story in each movie is based on a scenario in which a large asteroid is on a collision course with the earth. In each case, a space mission is launched with the goal of having astronauts plant nuclear bombs on the asteroid to destroy it before it can hit the earth and cause an “ELI” or Extinction Level Incident. An asteroid that collided with the earth some 65 million years ago near the Yucatan region is generally considered to be the ‘ELI’ that caused climate changes that eventually led to the extinction of the dinosaurs.

**The game scenario:** In our game, the astronauts have succeeded in planting the nukes on the asteroid and detonating them, but the asteroid was not broken into small enough pieces that would just burn up in the atmosphere. There are still fifty or so very large meteors falling towards earth that will cause extreme damage if any one of them hit the surface. This object of the game is to prevent any meteors from hitting the earth’s surface. If one does make it through your defenses, an ELI occurs, all life on earth will be wiped out, and the game is over.

Your primary defensive weapon is a powerful ground-based particle beam cannon that can vaporize falling meteors. You aim your particle beam using the mouse cursor as your targeting device, and you fire the particle beam by pressing the left mouse button. The meteors are different sizes, and each one requires from 1 to 4 particle beam hits to be destroyed, depending on the hardness of that particular meteor. However, occasionally one of the meteors will be a twin host. When a twin host is destroyed, it actually splits into two smaller, but still lethal meteors, which we call meteor babies. Both of these new meteor babies must also be destroyed to keep on playing.

Your secondary weapon is a fleet of tactical nuclear missiles (“nukes”) that can destroy all of the meteors that are visible on your tracking screen. However, detonating a nuclear missile in the atmosphere can spread dangerous fallout, so space defense command authority is reluctant to let you use them unless the situation gets critical and you are being overwhelmed by falling meteors. You don’t have access to a nuke at the start of the game, but if you manage to destroy 10 meteors, then you get one nuke added to your arsenal. You can fire the nuke by pressing the spacebar. It will immediately destroy all currently falling meteors but you do not get to add the number of meteors destroyed by the nuke to your score. After using your nuke, your nuke eligibility score goes back to zero and you have to destroy another ten meteors using the particle beam before you get access to another nuke.

---

## Specifications:
1) Set the applet size to be about 700 by 700 pixels.

2) **Meteor creation:** for each time step of your animation loop, there will be a 1% chance that a new meteor will be created. For this, you need to write a Meteor class. See the template starting on page 4 for some guidance. You will need to write a constructor method so that when a new Meteor is created, it assign values as follows:
 - A random x location on the screen between 50 and 650.
 - A fixed y location at the top of the screen, so that they all start at the top of the screen.
 - A randomly generated y velocity of between 1 and 25
 - A randomly generated radius of between 20 and 50 pixels.
 - An initial hardness value which is a random integer from 1 to 4. A Meteor with a hardness value of 1 needs only one hit to be destroyed. A hardness value of 2 requires two hits. A hardness value of 3 requires three hits, and a hardness value of 4 requires four hits to destroy it.  
 - A Boolean flag variable that determines if a particular meteor object is a twin or not. There should be a 10% chance that at instantiation a new Meteor object is a twin.

Also, each Meteor has an initial color of pure BLUE (RGB 0.0,0.0, 1.0) or RGB(0,0,255) assigned to it at instantiation. Each new Meteor created will be added to an ArrayList or Vector data structure. When a Meteor is destroyed by the particle beam, it is removed from the data structure.

>**NOTE:** you can make small variations from the above specs if you need to in order to make the game more playable or to solve speed problems. The bottom line is…make the game workable and playable.

3) **Meteor dynamics and appearance:** A meteor object will update its position in the updatePos() method by adding its y-velocity value to its y-position. A meteor starts out as completely blue in color (RGB 0.0, 0.0, 1.0). As it gets hit by the particle beam, it starts to turn red (RGB 1.0, 0.0, 0.0). NOTE: these parameter values are float primitives. We’ll use a linear interpolation between the blue and red values based on how much of a meteor’s initial hardness value is left. If a meteor started with an initial hardness value of ‘H’, and currently has ‘L’ hardness value left after being hit several times, then the current RGB color of the meteor should be: RGB (1.0 – L/H, 0.0, L/H).

 Adjust the above as required to suit your needs. If you want to convert to creating Color objects using the three int constructor instead, i.e. Color c – new Color( int,int, int) that is fine. Just adjust your code as needed.

4) **Firing the particle beam:** use methods of the MouseListener class to detect a mouse left button press. You can get the co-ordinates of the mouse at the same time by using the MouseEvent object’s getX() and getY() methods. The particle beam is fired from a fixed position at the bottom edge of the applet frame. When fired, a red line should be drawn from the particle beam position to the mouse’s current position on the screen.

The particle beam scores a hit if the distance from the co-ordinates of the mouse cursor to the circle that represents the Meteor is equal to or less than the radius of the meteor. Your getTarget() method should determine the distance between the mouse position and the center of the meteor. Each successful hit reduces the remaining hardness value of the Meteor by a value of 1. When the hardness value gets to zero, the meteor is destroyed and removed from the screen, and the player’s score increases by one.
5) Creating meteor baby twins: You will need a second constructor method that will take as an argument an existing Meteor object. See the Meteor class template on page 4. If the meteor that is destroyed was a twin host, then two new meteor babies should be created. The new meteor babies should be assigned values as follows:
The first meteor baby is created at the x location of the parent minus the parent’s radius value. So, if the parent was at x = 320, and had a radius of 50, then the first meteor baby x position should be at
320 – 50 = 280.  

The second meteor baby is created at the x location of the parent plus the parent’s radius value. So, if the parent was at x = 320, and had a radius of 50, then the second meteor baby x position should be at
320 + 50 = 370.  

- For both meteor babies, their y location should be the same as the parent’s y location when it was destroyed.
- For both meteor babies, their radius should be one-half of the parent Meteor.
- For both meteor babies, their initial hardness value should be the same as the parent’s hardness value.
- Finally, we’ll give the player a break…meteor babies cannot themselves be twins, so adjust the code in the second constructor to ensure this does not happen.

6) **Scoring:** display the score in the upper left corner of the screen. Score 1 point for every Meteor destroyed. If you destroy a twin, count one for destroying the parent and count 1 for each meteor baby that is destroyed. Remember, you don’t get any credit for meteors destroyed using the nuke.

7) **Nukes:** After you score 10 points you gain access to a nuke. This should be indicated visually on the screen somehow. You could have a message appear under the score that states “Nuke available now.”
The nuke is launched by pressing the space bar. You will have to use a keyPressed event listener to listen for this event. When it occurs, have your background color alternate quickly between red and white for 500 milliseconds to simulate a nuclear flash, and all existing meteors will be destroyed. If you use the nuke, the penalty for using it is that none of the meteors destroyed by the nuke get added to your overall score. After the nuke is used, your nuke eligibility score goes back to zero, no matter how many meteors you have already destroyed. So, you have to score another ten points using the particle beam to get another nuke.

8) **Game Over:** if the center of any Meteor gets down to the bottom edge of the screen the game is over. Display a message stating that “Extinction Level Incident has occurred…BYE-BYE!”.  
The “Victory score” should be 50 points. If the user gets to 50 points, they win the game, and you should print some sort of congratulatory message to the screen.

>Useful Hints:
To deal with flicker, you can use an image back buffer. We covered this in class, but if you want some more examples, try this [link](http://www.dgp.toronto.edu/~mjmcguff/learn/java/07-backbuffer/) from the University of Toronto

---

## Template for the Meteor class.
>NOTE: TBD means “to be done” by you…several of the methods that return values have placeholder values of “0” in them that you will need to replace with proper code.


``` java
public class Meteor
{
    // TBD: declare instance variables here…

	// Constructor, create a new Meteor located at the top of the screen
	public Meteor()
	{
		// TBD
	}
	// Constructor, given another Meteor as its parent object, create a new Meteor baby
	// based on the parent Meteor.
	public Meteor(Meteor mom, double deltaX)
	{
		// TBD
	}

	// Is this Meteor a twin Meteor?
	public boolean isTwin()
	{
		// TBD
	}	// Update the position of the Meteor based on its velocity
	public void updatePos()
	{
		// TBD
	}

	// Getter method for the x-position
	double getX()
	{
		// TBD
	}

	// Getter method for the y-position
	double getY()
	{
		// TBD
	}

	// Getter method for the radius
	double getRadius()
	{
		// TBD
		return radius;
	}
	//CONTINUED ON NEXT PAGE

	// The particle beam has scored a hit on this Meteor. Reduce the hardness value
	// of the Meteor by one and return the current amount of
	// hardness value remaining for this Meteor.  
	public int scoredHit()
	{
		// TBD
		return 0;
	}

	// Given a mouse (x, y) coordinate, decide if that location
	// was close enough to hit this Meteor.
	public boolean getTarget(double mouseX, double mouseY)
	{
		// TBD
		return false;
	}

}//end class Meteor ```

---

## Suggestions for Enhancing the Game
>(worth up to 2 bonus points)

- include background graphics such as a star field and the surface of the earth
- make the meteors have irregular “ rock” shapes rather than being just spherical
- add sound effects such as an explosion sound when a meteor is destroyed or background music
( NOTE: for background music find some royalty free sounds on the web that are not copyrighted, or if you are musically inclined, make up your own sound files)
- if player uses the nuke, increase the frequency that new meteors are created, or increase the speed of the falling meteors just a bit to make the game a bit more challenging. Don’t increase the speed so much that it makes the game unplayable…just make it a bit more challenging.

---

## Submission of Files

As some of you will probably be going for the bonus marks and will be adding sound and other graphics files, you should submit your entire Eclipse Project file in one zipped file to the drop box by the specified due date. If you worked in a team, only one submission is needed, but make sure each team member’s name appears in the doc header of each class.

The usual late penalties will apply. If it is up to 24 hours late, a 20% penalty will be applied. If it is from 24 to 48 hours late, a further 30% late penalty will be applied for a total 50% penalty. If it is more than 48 hours late, then a mark of zero will be assigned.
